﻿namespace Kalkulator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Clear = new System.Windows.Forms.Button();
            this.Logaritam = new System.Windows.Forms.Button();
            this.Jednako = new System.Windows.Forms.Button();
            this.Korijen = new System.Windows.Forms.Button();
            this.Tangens = new System.Windows.Forms.Button();
            this.Kosinus = new System.Windows.Forms.Button();
            this.Sinus = new System.Windows.Forms.Button();
            this.Dijeljenje = new System.Windows.Forms.Button();
            this.Puta = new System.Windows.Forms.Button();
            this.Minus = new System.Windows.Forms.Button();
            this.Plus = new System.Windows.Forms.Button();
            this.Tocka = new System.Windows.Forms.Button();
            this.Devet = new System.Windows.Forms.Button();
            this.Osam = new System.Windows.Forms.Button();
            this.Sedam = new System.Windows.Forms.Button();
            this.Sest = new System.Windows.Forms.Button();
            this.Pet = new System.Windows.Forms.Button();
            this.Tri = new System.Windows.Forms.Button();
            this.Dva = new System.Windows.Forms.Button();
            this.Cetiri = new System.Windows.Forms.Button();
            this.Jedan = new System.Windows.Forms.Button();
            this.Nula = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Clear
            // 
            this.Clear.Location = new System.Drawing.Point(222, 219);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(47, 32);
            this.Clear.TabIndex = 45;
            this.Clear.Text = "C";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // Logaritam
            // 
            this.Logaritam.Location = new System.Drawing.Point(275, 173);
            this.Logaritam.Name = "Logaritam";
            this.Logaritam.Size = new System.Drawing.Size(47, 40);
            this.Logaritam.TabIndex = 44;
            this.Logaritam.Text = "LOG";
            this.Logaritam.UseVisualStyleBackColor = true;
            this.Logaritam.Click += new System.EventHandler(this.Logaritam_Click);
            // 
            // Jednako
            // 
            this.Jednako.Location = new System.Drawing.Point(171, 85);
            this.Jednako.Name = "Jednako";
            this.Jednako.Size = new System.Drawing.Size(47, 32);
            this.Jednako.TabIndex = 43;
            this.Jednako.Text = "=";
            this.Jednako.UseVisualStyleBackColor = true;
            this.Jednako.Click += new System.EventHandler(this.Jednako_Click);
            // 
            // Korijen
            // 
            this.Korijen.Location = new System.Drawing.Point(275, 127);
            this.Korijen.Name = "Korijen";
            this.Korijen.Size = new System.Drawing.Size(47, 40);
            this.Korijen.TabIndex = 42;
            this.Korijen.Text = "SQRT";
            this.Korijen.UseVisualStyleBackColor = true;
            this.Korijen.Click += new System.EventHandler(this.Korijen_Click);
            // 
            // Tangens
            // 
            this.Tangens.Location = new System.Drawing.Point(275, 81);
            this.Tangens.Name = "Tangens";
            this.Tangens.Size = new System.Drawing.Size(47, 40);
            this.Tangens.TabIndex = 41;
            this.Tangens.Text = "TAN";
            this.Tangens.UseVisualStyleBackColor = true;
            this.Tangens.Click += new System.EventHandler(this.Tangens_Click);
            // 
            // Kosinus
            // 
            this.Kosinus.Location = new System.Drawing.Point(222, 173);
            this.Kosinus.Name = "Kosinus";
            this.Kosinus.Size = new System.Drawing.Size(47, 40);
            this.Kosinus.TabIndex = 40;
            this.Kosinus.Text = "COS";
            this.Kosinus.UseVisualStyleBackColor = true;
            this.Kosinus.Click += new System.EventHandler(this.Kosinus_Click);
            // 
            // Sinus
            // 
            this.Sinus.Location = new System.Drawing.Point(222, 127);
            this.Sinus.Name = "Sinus";
            this.Sinus.Size = new System.Drawing.Size(47, 40);
            this.Sinus.TabIndex = 39;
            this.Sinus.Text = "SIN";
            this.Sinus.UseVisualStyleBackColor = true;
            this.Sinus.Click += new System.EventHandler(this.Sinus_Click);
            // 
            // Dijeljenje
            // 
            this.Dijeljenje.Location = new System.Drawing.Point(222, 81);
            this.Dijeljenje.Name = "Dijeljenje";
            this.Dijeljenje.Size = new System.Drawing.Size(47, 40);
            this.Dijeljenje.TabIndex = 38;
            this.Dijeljenje.Text = "/";
            this.Dijeljenje.UseVisualStyleBackColor = true;
            this.Dijeljenje.Click += new System.EventHandler(this.Dijeljenje_Click);
            // 
            // Puta
            // 
            this.Puta.Location = new System.Drawing.Point(171, 219);
            this.Puta.Name = "Puta";
            this.Puta.Size = new System.Drawing.Size(47, 32);
            this.Puta.TabIndex = 37;
            this.Puta.Text = "*";
            this.Puta.UseVisualStyleBackColor = true;
            this.Puta.Click += new System.EventHandler(this.Puta_Click);
            // 
            // Minus
            // 
            this.Minus.Location = new System.Drawing.Point(171, 173);
            this.Minus.Name = "Minus";
            this.Minus.Size = new System.Drawing.Size(47, 40);
            this.Minus.TabIndex = 36;
            this.Minus.Text = "-";
            this.Minus.UseVisualStyleBackColor = true;
            this.Minus.Click += new System.EventHandler(this.Minus_Click);
            // 
            // Plus
            // 
            this.Plus.Location = new System.Drawing.Point(171, 127);
            this.Plus.Name = "Plus";
            this.Plus.Size = new System.Drawing.Size(47, 40);
            this.Plus.TabIndex = 35;
            this.Plus.Text = "+";
            this.Plus.UseVisualStyleBackColor = true;
            this.Plus.Click += new System.EventHandler(this.Plus_Click);
            // 
            // Tocka
            // 
            this.Tocka.Location = new System.Drawing.Point(118, 219);
            this.Tocka.Name = "Tocka";
            this.Tocka.Size = new System.Drawing.Size(47, 32);
            this.Tocka.TabIndex = 34;
            this.Tocka.Text = ",";
            this.Tocka.UseVisualStyleBackColor = true;
            this.Tocka.Click += new System.EventHandler(this.Tocka_Click);
            // 
            // Devet
            // 
            this.Devet.Location = new System.Drawing.Point(118, 81);
            this.Devet.Name = "Devet";
            this.Devet.Size = new System.Drawing.Size(47, 40);
            this.Devet.TabIndex = 33;
            this.Devet.Text = "9";
            this.Devet.UseVisualStyleBackColor = true;
            this.Devet.Click += new System.EventHandler(this.Devet_Click);
            // 
            // Osam
            // 
            this.Osam.Location = new System.Drawing.Point(65, 81);
            this.Osam.Name = "Osam";
            this.Osam.Size = new System.Drawing.Size(47, 40);
            this.Osam.TabIndex = 32;
            this.Osam.Text = "8";
            this.Osam.UseVisualStyleBackColor = true;
            this.Osam.Click += new System.EventHandler(this.Osam_Click);
            // 
            // Sedam
            // 
            this.Sedam.Location = new System.Drawing.Point(12, 81);
            this.Sedam.Name = "Sedam";
            this.Sedam.Size = new System.Drawing.Size(47, 40);
            this.Sedam.TabIndex = 31;
            this.Sedam.Text = "7";
            this.Sedam.UseVisualStyleBackColor = true;
            this.Sedam.Click += new System.EventHandler(this.Sedam_Click);
            // 
            // Sest
            // 
            this.Sest.Location = new System.Drawing.Point(118, 127);
            this.Sest.Name = "Sest";
            this.Sest.Size = new System.Drawing.Size(47, 40);
            this.Sest.TabIndex = 30;
            this.Sest.Text = "6";
            this.Sest.UseVisualStyleBackColor = true;
            this.Sest.Click += new System.EventHandler(this.Sest_Click);
            // 
            // Pet
            // 
            this.Pet.Location = new System.Drawing.Point(65, 127);
            this.Pet.Name = "Pet";
            this.Pet.Size = new System.Drawing.Size(47, 40);
            this.Pet.TabIndex = 29;
            this.Pet.Text = "5";
            this.Pet.UseVisualStyleBackColor = true;
            this.Pet.Click += new System.EventHandler(this.Pet_Click);
            // 
            // Tri
            // 
            this.Tri.Location = new System.Drawing.Point(118, 173);
            this.Tri.Name = "Tri";
            this.Tri.Size = new System.Drawing.Size(47, 40);
            this.Tri.TabIndex = 28;
            this.Tri.Text = "3";
            this.Tri.UseVisualStyleBackColor = true;
            this.Tri.Click += new System.EventHandler(this.Tri_Click);
            // 
            // Dva
            // 
            this.Dva.Location = new System.Drawing.Point(65, 173);
            this.Dva.Name = "Dva";
            this.Dva.Size = new System.Drawing.Size(47, 40);
            this.Dva.TabIndex = 27;
            this.Dva.Text = "2";
            this.Dva.UseVisualStyleBackColor = true;
            this.Dva.Click += new System.EventHandler(this.Dva_Click);
            // 
            // Cetiri
            // 
            this.Cetiri.Location = new System.Drawing.Point(12, 127);
            this.Cetiri.Name = "Cetiri";
            this.Cetiri.Size = new System.Drawing.Size(47, 40);
            this.Cetiri.TabIndex = 26;
            this.Cetiri.Text = "4";
            this.Cetiri.UseVisualStyleBackColor = true;
            this.Cetiri.Click += new System.EventHandler(this.Cetiri_Click);
            // 
            // Jedan
            // 
            this.Jedan.Location = new System.Drawing.Point(9, 173);
            this.Jedan.Name = "Jedan";
            this.Jedan.Size = new System.Drawing.Size(50, 40);
            this.Jedan.TabIndex = 25;
            this.Jedan.Text = "1";
            this.Jedan.UseVisualStyleBackColor = true;
            this.Jedan.Click += new System.EventHandler(this.Jedan_Click);
            // 
            // Nula
            // 
            this.Nula.Location = new System.Drawing.Point(9, 219);
            this.Nula.Name = "Nula";
            this.Nula.Size = new System.Drawing.Size(103, 32);
            this.Nula.TabIndex = 24;
            this.Nula.Text = "0";
            this.Nula.UseVisualStyleBackColor = true;
            this.Nula.Click += new System.EventHandler(this.Nula_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(310, 48);
            this.textBox1.TabIndex = 46;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(355, 262);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.Logaritam);
            this.Controls.Add(this.Jednako);
            this.Controls.Add(this.Korijen);
            this.Controls.Add(this.Tangens);
            this.Controls.Add(this.Kosinus);
            this.Controls.Add(this.Sinus);
            this.Controls.Add(this.Dijeljenje);
            this.Controls.Add(this.Puta);
            this.Controls.Add(this.Minus);
            this.Controls.Add(this.Plus);
            this.Controls.Add(this.Tocka);
            this.Controls.Add(this.Devet);
            this.Controls.Add(this.Osam);
            this.Controls.Add(this.Sedam);
            this.Controls.Add(this.Sest);
            this.Controls.Add(this.Pet);
            this.Controls.Add(this.Tri);
            this.Controls.Add(this.Dva);
            this.Controls.Add(this.Cetiri);
            this.Controls.Add(this.Jedan);
            this.Controls.Add(this.Nula);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Clear;
        private System.Windows.Forms.Button Logaritam;
        private System.Windows.Forms.Button Jednako;
        private System.Windows.Forms.Button Korijen;
        private System.Windows.Forms.Button Tangens;
        private System.Windows.Forms.Button Kosinus;
        private System.Windows.Forms.Button Sinus;
        private System.Windows.Forms.Button Dijeljenje;
        private System.Windows.Forms.Button Puta;
        private System.Windows.Forms.Button Minus;
        private System.Windows.Forms.Button Plus;
        private System.Windows.Forms.Button Tocka;
        private System.Windows.Forms.Button Devet;
        private System.Windows.Forms.Button Osam;
        private System.Windows.Forms.Button Sedam;
        private System.Windows.Forms.Button Sest;
        private System.Windows.Forms.Button Pet;
        private System.Windows.Forms.Button Tri;
        private System.Windows.Forms.Button Dva;
        private System.Windows.Forms.Button Cetiri;
        private System.Windows.Forms.Button Jedan;
        private System.Windows.Forms.Button Nula;
        private System.Windows.Forms.TextBox textBox1;
    }
}

