﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulator
{
    public partial class Form1 : Form
    {
        string Operacija="";
        double Prvi=0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Jedan_Click(object sender, EventArgs e)
        {
            if(textBox1.Text =="0"&& textBox1.Text!=null)
            {
                textBox1.Text = "1";
            }
            else
            {
                textBox1.Text = textBox1.Text + "1";
            }
        }

        private void Dva_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "0" && textBox1.Text != null)
            {
                textBox1.Text = "2";
            }
            else
            {
                textBox1.Text = textBox1.Text + "2";
            }
        }

        private void Tri_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "0" && textBox1.Text != null)
            {
                textBox1.Text = "3";
            }
            else
            {
                textBox1.Text = textBox1.Text + "3";
            }
        }

        private void Cetiri_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "0" && textBox1.Text != null)
            {
                textBox1.Text = "4";
            }
            else
            {
                textBox1.Text = textBox1.Text + "4";
            }
        }

        private void Pet_Click(object sender, EventArgs e)
        {
            if(textBox1.Text=="0"&& textBox1.Text!=null)
            {
                textBox1.Text = "5";
            }
            else
            {
                textBox1.Text = textBox1.Text + "5";
            }
        }

        private void Sest_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "0" && textBox1.Text != null)
            {
                textBox1.Text = "6";
            }
            else
            {
                textBox1.Text = textBox1.Text + "6";
            }
        }

        private void Sedam_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "0" && textBox1.Text != null)
            {
                textBox1.Text = "7";
            }
            else
            {
                textBox1.Text = textBox1.Text + "7";
            }
        }

        private void Osam_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "0" && textBox1.Text != null)
            {
                textBox1.Text = "8";
            }
            else
            {
                textBox1.Text = textBox1.Text + "8";
            }
        }

        private void Devet_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "0" && textBox1.Text != null)
            {
                textBox1.Text = "9";
            }
            else
            {
                textBox1.Text = textBox1.Text + "9";
            }
        }

        private void Nula_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "0" && textBox1.Text != null)
            {
                textBox1.Text = "0";
            }
            else
            {
                textBox1.Text = textBox1.Text + "0";
            }
        }

        private void Tocka_Click(object sender, EventArgs e)
        {
            if (!textBox1.Text.Contains(','))
            {
                textBox1.Text += ",";
            }
            
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox1.Text = "0";
        }

        private void Sinus_Click(object sender, EventArgs e)
        {
            textBox1.Text = (Math.Sin(Double.Parse(textBox1.Text))).ToString();
        }

        private void Kosinus_Click(object sender, EventArgs e)
        {
            textBox1.Text = (Math.Cos(Double.Parse(textBox1.Text))).ToString();
        }

        private void Tangens_Click(object sender, EventArgs e)
        {
            textBox1.Text = (Math.Tan(Double.Parse(textBox1.Text))).ToString();
        }

        private void Korijen_Click(object sender, EventArgs e)
        {
            textBox1.Text = (Math.Sqrt(Double.Parse(textBox1.Text))).ToString();
        }

        private void Logaritam_Click(object sender, EventArgs e)
        {
            textBox1.Text = (Math.Log(Double.Parse(textBox1.Text))).ToString();
        }

        private void Puta_Click(object sender, EventArgs e)
        {
            Prvi = Convert.ToDouble(textBox1.Text);
            textBox1.Text = "0";
            Operacija = "*";
        }

        private void Minus_Click(object sender, EventArgs e)
        {
            Prvi = Convert.ToDouble(textBox1.Text);
            textBox1.Text = "0";
            Operacija = "-";
        }

        private void Plus_Click(object sender, EventArgs e)
        {
            Prvi = Convert.ToDouble(textBox1.Text);
            textBox1.Text = "0";
            Operacija = "+";
        }

        private void Jednako_Click(object sender, EventArgs e)
        {
            double Drugi;
            double Rezultat;
            Drugi = Convert.ToDouble(textBox1.Text);
            if(Operacija=="+")
            {
                Rezultat = Prvi + Drugi;
                textBox1.Text = Convert.ToString(Rezultat);
                Prvi = Rezultat;
            }
            if (Operacija == "*")
            {
                Rezultat = Prvi * Drugi;
                textBox1.Text = Convert.ToString(Rezultat);
                Prvi = Rezultat;
            }
            if (Operacija == "-")
            {
                Rezultat = Prvi - Drugi;
                textBox1.Text = Convert.ToString(Rezultat);
                Prvi = Rezultat;
            }
            if (Operacija == "/")
            {
                Rezultat = (Prvi / Drugi);
                textBox1.Text = Convert.ToString(Rezultat);
                Prvi = Rezultat;
            }
        }

        private void Dijeljenje_Click(object sender, EventArgs e)
        {
            Prvi = Convert.ToDouble(textBox1.Text);
            textBox1.Text = "0";
            Operacija = "/";
        }
    }
}
